<?php

namespace Pizza\Pizza;

use Pizza\Models\PizzaContainer;
use Pizza\Models\PizzaModel;
use Pizza\Pizza\Exceptions\PizzaException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Pizza
 * @package Pizza\Pizza
 */
class Pizza implements PizzaInterface, LoggerAwareInterface
{
    /**
     * @var NullLogger
     */
    private $logger;
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * Pizza constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->logger = new NullLogger();
    }

    /**
     * @return array|PizzaContainer
     * @throws PizzaException
     */
    public function getAllPizzas()
    {
        $SQL = "SELECT * FROM pizzas";

        $query = $this->pdo->prepare($SQL);

        if (!$query->execute()) {
            $this->logger->error(
                "Could not execute query",
                array(
                    "query" => $SQL
                )
            );
            throw new PizzaException("Could not retrieve pizzas at this time, please try again later", 500);
        }

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if (!$result) {
            $this->logger->info(
                "No results returned",
                array(
                    "query" => $SQL
                )
            );
            // return empty container...
            return new PizzaContainer();
        }

        $pizzaContainer = new PizzaContainer();
        $pizzaArray = array();

        foreach ($result as $pizza) {
            $pizzaModel = new PizzaModel();
            $pizzaModel->setId($pizza['pizza_id']);
            $pizzaModel->setName($pizza['name']);
            $pizzaModel->setDescription($pizza['description']);

            $pizzaArray[] = $pizzaModel;
        }

        $pizzaContainer->setPizzas($pizzaArray);
        return $pizzaContainer->toArray();
    }

    /**
     * @param PizzaModel $pizzaModel
     * @return PizzaModel
     * @throws PizzaException
     */
    public function createPizza(PizzaModel $pizzaModel)
    {
        $SQL = "INSERT INTO pizzas (name, description) values (?, ?)";

        $query = $this->pdo->prepare($SQL);

        if (!$query->execute(
            array(
                    $pizzaModel->getName(),
                    $pizzaModel->getDescription()
                )
        )
        ) {
            $this->logger->error(
                "Could not execute query",
                array(
                    "query" => $SQL
                )
            );
            throw new PizzaException("Could not create pizza at this time, please try again later", 500);
        }

        if (!$this->pdo->lastInsertId()) {
            $this->logger->info(
                "No insert ID found, must not have been created",
                array(
                    "query" => $SQL,
                    "vars"  =>
                        array(
                            $pizzaModel->getName(),
                            $pizzaModel->getDescription()
                        )
                )
            );
            throw new PizzaException("Could not create topping at this time, please try again later.", 500);
        }

        $pizzaModel->setId($this->pdo->lastInsertId());
        return $pizzaModel;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
