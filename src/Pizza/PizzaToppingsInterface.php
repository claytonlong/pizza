<?php

namespace Pizza\Pizza;

use Pizza\Models\PizzaModel;
use Pizza\Models\ToppingContainer;
use Pizza\Models\ToppingModel;
use Pizza\Pizza\Exceptions\PizzaException;

/**
 * Interface PizzaToppingsInterface
 * @package Pizza\Pizza
 */
interface PizzaToppingsInterface
{
    /**
     * @param PizzaModel $pizzaModel
     * @return ToppingContainer
     * @throws PizzaException
     */
    public function getToppings(PizzaModel $pizzaModel);

    /**
     * @param PizzaModel $pizzaModel
     * @param ToppingModel $toppingModel
     * @return PizzaModel
     * @throws PizzaException
     */
    public function addTopping(PizzaModel $pizzaModel, ToppingModel $toppingModel);
}
