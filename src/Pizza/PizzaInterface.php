<?php

namespace Pizza\Pizza;

use Pizza\Models\PizzaModel;
use Pizza\Pizza\Exceptions\PizzaException;

/**
 * Interface PizzaInterface
 * @package Pizza\Pizza
 */
interface PizzaInterface
{
    /**
     * @return Pizza
     * @throws PizzaException
     */
    public function getAllPizzas();

    /**
     * @param PizzaModel $pizzaModel
     * @return bool
     * @throws PizzaException
     */
    public function createPizza(PizzaModel $pizzaModel);
}
