<?php

namespace Pizza\Pizza;

use Pizza\Models\PizzaModel;
use Pizza\Models\ToppingContainer;
use Pizza\Models\ToppingModel;
use Pizza\Pizza\Exceptions\PizzaException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class PizzaToppings
 * @package Pizza\Pizza
 */
class PizzaToppings implements LoggerAwareInterface, PizzaToppingsInterface
{
    /**
     * @var \PDO
     */
    private $pdo;
    /**
     * @var NullLogger
     */
    private $logger;

    /**
     * PizzaToppings constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->logger = new NullLogger();
    }

    /**
     * @param PizzaModel $pizzaModel
     * @return ToppingContainer
     * @throws PizzaException
     */
    public function getToppings(PizzaModel $pizzaModel)
    {
         $SQL = "SELECT pz.topping_id, t.name FROM pizza_toppings pz 
                  inner join toppings t on t.topping_id = pz.topping_id  where pizza_id = ?";

        $query = $this->pdo->prepare($SQL);

        if (!$query->execute(array($pizzaModel->getId()))) {
            $this->logger->error(
                "Could not execute query",
                array(
                    "query" => $SQL
                )
            );
            throw new PizzaException("Could not retrieve pizzas at this time, please try again later", 500);
        }

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if (!$result) {
            $this->logger->info(
                "No results returned",
                array(
                    "query" => $SQL
                )
            );
            // return empty container...
            return new ToppingContainer();
        }

        $toppingContainer = new ToppingContainer();
        $pizzaToppingArray = array();

        foreach ($result as $pizzaTopping) {
            $topping = new ToppingModel();
            $topping->setName($pizzaTopping['name']);
            $topping->setId($pizzaTopping['topping_id']);
            $pizzaToppingArray[] = $topping;
        }

        $toppingContainer->setToppings($pizzaToppingArray);

        return $toppingContainer;
    }

    /**
     * @param PizzaModel $pizzaModel
     * @param ToppingModel $toppingModel
     * @return PizzaModel
     * @throws PizzaException
     */
    public function addTopping(PizzaModel $pizzaModel, ToppingModel $toppingModel)
    {
        $toppingContainer = new ToppingContainer();
        $toppingContainer->setToppings(array($toppingModel));

        $pizzaModel->setToppings($toppingContainer);

        if ($this->isToppingAlreadyOnPizza($pizzaModel, $toppingModel)) {
            return $pizzaModel;
        }

        $SQL = "INSERT INTO pizza_toppings (pizza_id, topping_id) VALUES (?, ?)";

        $query = $this->pdo->prepare($SQL);

        if (!$query->execute(
            array(
                $pizzaModel->getId(),
                $toppingModel->getId()
            )
        )
        ) {
            $this->logger->error(
                "Could not execute query",
                array(
                    "query"     => $SQL,
                    "errorInfo" => $query->errorInfo()
                )
            );
            throw new PizzaException(
                "Could not add topping to pizza at this time, please try again later.",
                500
            );
        }

        return $pizzaModel;
    }

    /**
     * @param PizzaModel $pizzaModel
     * @param ToppingModel $toppingModel
     * @return bool
     * @throws PizzaException
     */
    private function isToppingAlreadyOnPizza(PizzaModel $pizzaModel, ToppingModel $toppingModel)
    {
        $SQL = "SELECT * FROM pizza_toppings where pizza_id = ? and topping_id = ?";

        $query = $this->pdo->prepare($SQL);

        if (!$query->execute(
            array(
                $pizzaModel->getId(),
                $toppingModel->getId()
            )
        )
        ) {
            $this->logger->error(
                "Could not execute query",
                array(
                    "query"     => $SQL,
                    "errorInfo" => $query->errorInfo()
                )
            );
            throw new PizzaException(
                "Could not add topping to pizza at this time, please try again later.",
                500
            );
        }

        if ($query->rowCount() > 0) {
            return true;
        }

        return false;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
