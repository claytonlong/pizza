<?php

namespace Pizza\Toppings;

use Pizza\Models\ToppingContainer;
use Pizza\Models\ToppingModel;
use Pizza\Toppings\Exceptions\ToppingException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Toppings
 * @package Pizza\Toppings
 */
class Toppings implements ToppingsInterface, LoggerAwareInterface
{
    /**
     * @var NullLogger
     */
    private $logger;
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * Toppings constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->logger = new NullLogger();
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return ToppingContainer
     * @throws ToppingException
     */
    public function getAllToppings()
    {
        $SQL = "SELECT * FROM toppings";

        $query = $this->pdo->prepare($SQL);

        if (!$query->execute()) {
            $this->logger->error(
                "Could not execute query",
                array(
                    "query" => $SQL
                )
            );
            throw new ToppingException("Could not retrieve toppings at this time, please try again later", 500);
        }

        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if (!$result) {
            $this->logger->info(
                "No results returned",
                array(
                    "query" => $SQL
                )
            );
            // return empty container...
            return new ToppingContainer();
        }

        $toppingContainer = new ToppingContainer();
        $toppingArray = array();

        foreach ($result as $topping) {
            $toppingModel = new ToppingModel();
            $toppingModel->setId($topping['topping_id']);
            $toppingModel->setName($topping['name']);

            $toppingArray[] = $toppingModel;
        }

        $toppingContainer->setToppings($toppingArray);
        return $toppingContainer;
    }

    /**
     * @param ToppingModel $toppingModel
     * @return ToppingModel
     * @throws ToppingException
     */
    public function createTopping(ToppingModel $toppingModel)
    {
        $SQL = "INSERT INTO toppings (name) VALUES (?)";

        $query = $this->pdo->prepare($SQL);

        if (!$query->execute(array($toppingModel->getName()))) {
            $this->logger->error(
                "Could not execute query",
                array(
                    "query" => $SQL,
                    "vars"  => $toppingModel->getName()
                )
            );
            throw new ToppingException("Could not create topping at this time, please try again later.", 500);
        }

        if (!$this->pdo->lastInsertId()) {
            $this->logger->info(
                "No insert ID found, must not have been created",
                array(
                    "query" => $SQL,
                    "vars"  => $toppingModel->getName()
                )
            );
            throw new ToppingException("Could not create topping at this time, please try again later.", 500);
        }

        /*
         * ID was set so now we know it was created, let's set it in the model and return.
         */
        $toppingModel->setId($this->pdo->lastInsertId());

        return $toppingModel;
    }
}
