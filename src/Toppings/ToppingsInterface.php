<?php

namespace Pizza\Toppings;

use Pizza\Models\ToppingContainer;
use Pizza\Models\ToppingModel;
use Pizza\Toppings\Exceptions\ToppingException;

/**
 * Interface ToppingsInterface
 * @package Pizza\Toppings
 */
interface ToppingsInterface
{
    /**
     * @return ToppingContainer
     */
    public function getAllToppings();

    /**
     * @param ToppingModel $toppingModel
     * @return bool
     * @throws ToppingException
     */
    public function createTopping(ToppingModel $toppingModel);
}
