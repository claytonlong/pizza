CREATE TABLE `pizza_toppings` (
  `pizza_id` int(11) NOT NULL,
  `topping_id` int(11) NOT NULL,
  KEY `topping_id` (`topping_id`),
  KEY `pizza_id` (`pizza_id`),
  CONSTRAINT `fk_pizza_id` FOREIGN KEY (`pizza_id`) REFERENCES `pizzas` (`pizza_id`),
  CONSTRAINT `fk_topping_id` FOREIGN KEY (`topping_id`) REFERENCES `toppings` (`topping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8