<?php
// Routes

$app->get(
    '/',
    function ($request, $response, $args) {
        $array = array(
        "/toppings" => [
            "GET" => [
                "Description" => "List toppings",
                "Required"    => []
            ],
            "POST" => [
                "Description" => "Create a topping",
                "Required"    => [
                    "in"     => "body",
                    "format" => "JSON",
                    "example" => [
                        "topping" => [
                            "name" => "Pepperoni"
                        ]
                    ]
                ]
            ]
        ],
        "/pizzas" => [
            "GET" => [
                "Description" => "Retrieves a list of all pizza's",
                "Required"    => []
            ],
            "POST" => [
                "Description" => "Creates a pizza",
                "Required"    => [
                    "in"     => "body",
                    "format" => "JSON",
                    "example" => [
                        "pizza" => [
                            "name" => "Pepperoni Pizza",
                            "description" => "The Zeus of Pizza's"
                        ]
                    ]
                ]
            ]
        ],
        "/pizzas/{pizzaId}/toppings" => [
            "GET" => [
                "Description" => "Retrieves the toppings on a pizza",
                "Required"    => []
            ],
            "POST" => [
                "Description" => "Adds a topping to the pizza",
                "Required"    => [
                    "in"     => "body",
                    "format" => "JSON",
                    "example" => [
                        "topping_id" => 1
                    ]
                ]
            ]
        ]
        );

        return $response->withJson($array, 200, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }
);

$app->get("/pizzas", "PizzaView:getAllPizzas");
$app->post("/pizzas", "PizzaView:createPizza");

$app->get("/pizzas/{pizzaId}/toppings", "PizzaToppingsView:listToppingsOnPizza");
$app->post("/pizzas/{pizzaId}/toppings", "PizzaToppingsView:addToppingToPizza");

$app->get("/toppings", "ToppingView:getAllToppings");
$app->post("/toppings", "ToppingView:createTopping");
