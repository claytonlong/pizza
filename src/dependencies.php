<?php
// DIC configuration

$container = $app->getContainer();

// monolog
/**
 * @param $c
 * @return \Monolog\Logger
 */
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

/**
 * @param \Slim\Container $c
 * @return PDO
 */
$container['db'] = function ($c) {
    $pdo = new PDO(
        sprintf("mysql:host=%s;dbname=%s", $c->get('settings')['db']['host'], $c->get('settings')['db']['name']),
        $c->get('settings')['db']['user'],
        $c->get('settings')['db']['pass']
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

/**
 * @param $c
 * @return \Pizza\Pizza\Pizza
 */
$container['Pizza'] = function ($c) {
    // lol this namespace turned out perfect
    $pizza = new \Pizza\Pizza\Pizza($c['db']);
    $pizza->setLogger($c['logger']);
    return $pizza;
};

/**
 * @param $c
 * @return \Pizza\HTTP\Controllers\PizzaController
 */
$container['PizzaController'] = function ($c) {
    return new \Pizza\HTTP\Controllers\PizzaController($c['Pizza']);
};

/**
 * @param $c
 * @return \Pizza\HTTP\Views\PizzaView
 */
$container['PizzaView'] = function ($c) {
    return new \Pizza\HTTP\Views\PizzaView($c['PizzaController']);
};

/**
 * @param $c
 * @return \Pizza\Pizza\PizzaToppings
 */
$container['PizzaToppings'] = function ($c) {
    $pizzaToppings = new \Pizza\Pizza\PizzaToppings($c['db']);
    $pizzaToppings->setLogger($c['logger']);
    return $pizzaToppings;
};

/**
 * @param $c
 * @return \Pizza\HTTP\Controllers\PizzaToppingController
 */
$container['PizzaToppingsController'] = function ($c) {
    return new \Pizza\HTTP\Controllers\PizzaToppingController($c['PizzaToppings']);
};

/**
 * @param $c
 * @return \Pizza\HTTP\Views\PizzaToppingsView
 */
$container['PizzaToppingsView'] = function ($c) {
    return new \Pizza\HTTP\Views\PizzaToppingsView($c['PizzaToppingsController']);
};

/**
 * @param $c
 * @return \Pizza\Toppings\Toppings
 */
$container['Toppings'] = function ($c) {
    $toppings = new \Pizza\Toppings\Toppings($c['db']);
    $toppings->setLogger($c['logger']);
    return $toppings;
};

/**
 * @param $c
 * @return \Pizza\HTTP\Controllers\ToppingsController
 */
$container['ToppingsController'] = function ($c) {
    return new \Pizza\HTTP\Controllers\ToppingsController($c['Toppings']);
};

/**
 * @param $c
 * @return \Pizza\HTTP\Views\ToppingView
 */
$container['ToppingView'] = function ($c) {
    return new \Pizza\HTTP\Views\ToppingView($c['ToppingsController']);
};
