<?php

namespace Pizza\HTTP\Controllers;

use Pizza\Models\PizzaModel;
use Pizza\Models\ResponseModel;
use Pizza\Pizza\Exceptions\PizzaException;
use Pizza\Pizza\PizzaInterface;
use Slim\Http\Request;
use Valitron\Validator;

/**
 * Class PizzaController
 * @package Pizza\HTTP\Controllers
 */
class PizzaController
{
    /**
     * @var PizzaInterface
     */
    private $pizza;
    /**
     * @var
     */
    private $validateErrorMessage;

    /**
     * PizzaController constructor.
     * @param PizzaInterface $pizza
     */
    public function __construct(PizzaInterface $pizza)
    {
        $this->pizza = $pizza;
    }

    /**
     * @param Request $request
     * @param array $args
     * @return ResponseModel
     */
    public function listPizzas(Request $request, array $args)
    {
        $response = new ResponseModel();

        try {
            $pizzas = $this->pizza->getAllPizzas();
        } catch (PizzaException $e) {
            $response->setData(
                array(
                    "message" => $e->getMessage(),
                    "code"    => $e->getCode(),
                )
            );
            $response->setStatusCode(500);

            return $response;
        }

        $response->setData($pizzas);
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * @param Request $request
     * @param array $args
     * @return ResponseModel
     */
    public function createPizza(Request $request, array $args)
    {
        $response = new ResponseModel();

        if (!$this->validateRequest($request)) {
            $response->setData(
                array(
                    "message"   => $this->validateErrorMessage,
                    "code"      => 1001
                )
            );
            $response->setStatusCode(400);
            return $response;
        }

        $requestPizza = $request->getParsedBodyParam("pizza");

        $pizza = new PizzaModel();
        $pizza->setName($requestPizza['name']);
        $pizza->setDescription($requestPizza['description']);

        try {
            /** @var PizzaModel $pizza */
            $pizza = $this->pizza->createPizza($pizza);
        } catch (PizzaException $e) {
            $response->setData(
                array(
                    "message" => $e->getMessage(),
                    "code"    => $e->getCode(),
                )
            );
            $response->setStatusCode(400);
            return $response;
        }

        $response->setData($pizza->toArray());
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function validateRequest(Request $request)
    {
        $v = new Validator($request->getParsedBody());
        $v->rule("required", ["pizza"])->message("Pizza array parent is required");
        $v->rule("required", ["pizza.name", "pizza.description"])
            ->message("Pizza name and description are critical to fresh pizza's");

        if ($v->validate()) {
            return true;
        }

        $this->validateErrorMessage = $v->errors();
        return false;
    }
}
