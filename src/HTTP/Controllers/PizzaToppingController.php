<?php

namespace Pizza\HTTP\Controllers;

use Pizza\Models\PizzaModel;
use Pizza\Models\ResponseModel;
use Pizza\Models\ToppingModel;
use Pizza\Pizza\Exceptions\PizzaException;
use Pizza\Pizza\PizzaToppingsInterface;
use Slim\Http\Request;
use Valitron\Validator;

/**
 * Class PizzaToppingController
 * @package Pizza\HTTP\Controllers
 */
class PizzaToppingController
{
    /**
     * @var PizzaToppingsInterface
     */
    private $pizzaToppings;
    /**
     * @var
     */
    private $validateErrorMessage;

    /**
     * PizzaToppingController constructor.
     * @param PizzaToppingsInterface $pizzaToppings
     */
    public function __construct(PizzaToppingsInterface $pizzaToppings)
    {
        $this->pizzaToppings = $pizzaToppings;
    }

    /**
     * @param Request $request
     * @param array $args
     * @return ResponseModel
     */
    public function listToppingsOnPizza(Request $request, array $args)
    {
        $response = new ResponseModel();

        $pizzaModel = new PizzaModel();
        $pizzaModel->setId($args['pizzaId']);

        try {
            $pizzaToppings = $this->pizzaToppings->getToppings($pizzaModel);
        } catch (PizzaException $e) {
            $response->setData(
                array(
                    "message" => $e->getMessage(),
                    "code"    => $e->getCode()
                )
            );
            $response->setStatusCode(500);
            return $response;
        }

        $response->setData($pizzaToppings->toArray());
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * @param Request $request
     * @param array $args
     * @return ResponseModel
     */
    public function addToppingToPizza(Request $request, array $args)
    {
        $response = new ResponseModel();

        if (!$this->validateRequest($request)) {
            $response->setData(
                array(
                    "message"   => $this->validateErrorMessage,
                    "code"      => 1001
                )
            );
            $response->setStatusCode(400);
            return $response;
        }

        $pizza = new PizzaModel();
        $pizza->setId($args['pizzaId']);

        $topping = new ToppingModel();
        $topping->setId($request->getParsedBodyParam("topping_id"));

        try {
            $this->pizzaToppings->addTopping($pizza, $topping);
        } catch (PizzaException $e) {
            $response->setData(
                array(
                    "message" => $e->getMessage(),
                    "code"    => $e->getCode()
                )
            );
            $response->setStatusCode(500);
            return $response;
        }

        $response->setData(
            array(
                "message" => "successfully added topping to pizza",
                "code"    => 200
            )
        );
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function validateRequest(Request $request)
    {
        $v = new Validator($request->getParsedBody());
        $v->rule("required", ["topping_id"])->message("topping_id is required!");

        if ($v->validate()) {
            return true;
        }

        $this->validateErrorMessage = $v->errors();
        return false;
    }
}
