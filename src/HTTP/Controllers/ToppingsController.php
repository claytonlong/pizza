<?php

namespace Pizza\HTTP\Controllers;

use Pizza\Models\ResponseModel;
use Pizza\Models\ToppingModel;
use Pizza\Toppings\Exceptions\ToppingException;
use Pizza\Toppings\ToppingsInterface;
use Slim\Http\Request;
use Valitron\Validator;

/**
 * Class ToppingsController
 * @package Pizza\HTTP\Controllers
 */
class ToppingsController
{
    /**
     * @var
     */
    private $validateErrorMessage;
    /**
     * @var ToppingsInterface
     */
    private $toppings;

    /**
     * ToppingsController constructor.
     * @param ToppingsInterface $toppings
     */
    public function __construct(ToppingsInterface $toppings)
    {
        $this->toppings = $toppings;
    }

    /**
     * @param Request $request
     * @param array $args
     * @return ResponseModel
     */
    public function listToppings(Request $request, array $args)
    {
        $response = new ResponseModel();

        try {
            $toppings = $this->toppings->getAllToppings();
        } catch (ToppingException $e) {
            $response->setData(
                array(
                    "message" => $e->getMessage(),
                    "code"    => $e->getCode(),
                )
            );
            $response->setStatusCode(400);
            return $response;
        }

        $response->setData($toppings->toArray());
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * @param Request $request
     * @param array $args
     * @return ResponseModel
     */
    public function createTopping(Request $request, array $args)
    {
        $response = new ResponseModel;

        if (!$this->validateRequest($request)) {
            $response->setData(
                array(
                    "message"   => $this->validateErrorMessage,
                    "code"      => 1001
                )
            );
            $response->setStatusCode(400);
            return $response;
        }

        $topping = new ToppingModel();
        $topping->setName($request->getParsedBodyParam("topping")['name']);

        try {
            /** @var ToppingModel $topping */
            $topping = $this->toppings->createTopping($topping);
        } catch (ToppingException $e) {
            $response->setData(
                array(
                    "message" => $e->getMessage(),
                    "code"    => $e->getCode(),
                )
            );
            $response->setStatusCode(500);
            return $response;
        }

        $response->setData($topping->toArray());
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function validateRequest(Request $request)
    {
        $v = new Validator($request->getParsedBody());
        $v->rule("required", ["topping"])->message("'Topping' parent array is required!");
        $v->rule("required", ["topping.name"])->message("You can't have a topping without a name silly goose");

        if ($v->validate()) {
            return true;
        }

        $this->validateErrorMessage = $v->errors();
        return false;
    }
}
