<?php

namespace Pizza\HTTP\Views;

use Pizza\HTTP\Controllers\PizzaController;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PizzaView
 * @package Pizza\HTTP\Views
 */
class PizzaView
{
    /**
     * @var PizzaController
     */
    private $pizzaController;

    /**
     * PizzaView constructor.
     * @param PizzaController $pizzaController
     */
    public function __construct(PizzaController $pizzaController)
    {
        $this->pizzaController = $pizzaController;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function getAllPizzas(Request $request, Response $response, array $args)
    {
        /*
         * I've setup the ResponseModel to include the data and status code,
         * the reason for doing this is that in our view, we may want to define how
         * the data is outputted. In this example we use JSON, however it could change
         * to XML, JSON_PRETTY_PRINT, etc...
         */
        $responseModel = $this->pizzaController->listPizzas($request, $args);

        return $response->withJson(
            $responseModel->getData(),
            $responseModel->getStatusCode()
        );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function createPizza(Request $request, Response $response, array $args)
    {
        $responseModel = $this->pizzaController->createPizza($request, $args);

        return $response->withJson(
            $responseModel->getData(),
            $responseModel->getStatusCode()
        );
    }
}
