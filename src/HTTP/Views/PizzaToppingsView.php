<?php

namespace Pizza\HTTP\Views;

use Pizza\HTTP\Controllers\PizzaToppingController;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class PizzaToppingsView
 * @package Pizza\HTTP\Views
 */
class PizzaToppingsView
{
    /**
     * @var PizzaToppingController
     */
    private $pizzaToppingController;

    /**
     * PizzaToppingsView constructor.
     * @param PizzaToppingController $pizzaToppingController
     */
    public function __construct(PizzaToppingController $pizzaToppingController)
    {
        $this->pizzaToppingController = $pizzaToppingController;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function addToppingToPizza(Request $request, Response $response, array $args)
    {
        $responseModel = $this->pizzaToppingController->addToppingToPizza($request, $args);

        return $response->withJson(
            $responseModel->getData(),
            $responseModel->getStatusCode()
        );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function listToppingsOnPizza(Request $request, Response $response, array $args)
    {
        $responseModel = $this->pizzaToppingController->listToppingsOnPizza($request, $args);

        return $response->withJson(
            $responseModel->getData(),
            $responseModel->getStatusCode()
        );
    }
}
