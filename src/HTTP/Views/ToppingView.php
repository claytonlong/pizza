<?php

namespace Pizza\HTTP\Views;

use Pizza\HTTP\Controllers\ToppingsController;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ToppingView
 * @package Pizza\HTTP\Views
 */
class ToppingView
{
    /**
     * @var ToppingsController
     */
    private $toppingsController;

    /**
     * ToppingView constructor.
     * @param ToppingsController $toppingsController
     */
    public function __construct(ToppingsController $toppingsController)
    {
        $this->toppingsController = $toppingsController;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function getAllToppings(Request $request, Response $response, array $args)
    {
        $responseModel = $this->toppingsController->listToppings($request, $args);

        return $response->withJson(
            $responseModel->getData(),
            $responseModel->getStatusCode()
        );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function createTopping(Request $request, Response $response, array $args)
    {
        $responseModel = $this->toppingsController->createTopping($request, $args);

        return $response->withJson(
            $responseModel->getData(),
            $responseModel->getStatusCode()
        );
    }
}
