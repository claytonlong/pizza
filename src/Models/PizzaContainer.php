<?php

namespace Pizza\Models;

/**
 * Class PizzaContainer
 * @package Pizza\Models
 */
class PizzaContainer
{
    /**
     * @var PizzaModel[]
     */
    private $pizzas;

    /**
     * @return array
     */
    public function toArray()
    {
        $pizzaArray = array();
        foreach ($this->getPizzas() as $pizzaModel) {
            $pizzaArray[] = array(
                "id"          => $pizzaModel->getId(),
                "name"        => $pizzaModel->getName(),
                "description" => $pizzaModel->getDescription()
            );
        }

        return $pizzaArray;
    }

    /**
     * @return PizzaModel[]
     */
    public function getPizzas()
    {
        return $this->pizzas;
    }

    /**
     * @param PizzaModel[] $pizzas
     */
    public function setPizzas($pizzas)
    {
        $this->pizzas = $pizzas;
    }
}
