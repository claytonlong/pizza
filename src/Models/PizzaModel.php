<?php

namespace Pizza\Models;

/**
 * Class PizzaModel
 * @package Pizza\Models
 */
class PizzaModel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $description;
    /**
     * @var ToppingContainer
     */
    private $toppings;

    /**
     * PizzaModel constructor.
     */
    public function __construct()
    {
        $this->setToppings(new ToppingContainer());
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            "id"          => $this->getId(),
            "name"        => $this->getName(),
            "description" => $this->getDescription(),
            "toppings"    => $this->getToppings()->toArray()
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return ToppingContainer
     */
    public function getToppings()
    {
        return $this->toppings;
    }

    /**
     * @param ToppingContainer $toppings
     */
    public function setToppings($toppings)
    {
        $this->toppings = $toppings;
    }
}
