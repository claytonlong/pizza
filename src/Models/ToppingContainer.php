<?php

namespace Pizza\Models;

/**
 * Class ToppingContainer
 * @package Pizza\Models
 */
class ToppingContainer
{
    /**
     * @var ToppingModel[]
     */
    private $toppings;

    /**
     * @return array
     */
    public function toArray()
    {
        $toppingArray = array();

        if ($this->getToppings() === null) {
            return array();
        }

        foreach ($this->getToppings() as $topping) {
            $toppingArray[] = array(
                "id"          => $topping->getId(),
                "name"        => $topping->getName()
            );
        }

        return $toppingArray;
    }
    /**
     * @return ToppingModel[]
     */
    public function getToppings()
    {
        return $this->toppings;
    }

    /**
     * @param ToppingModel
     */
    public function setToppings($toppings)
    {
        $this->toppings = $toppings;
    }
}
