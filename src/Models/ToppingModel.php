<?php

namespace Pizza\Models;

/**
 * Class ToppingModel
 * @package Pizza\Models
 */
class ToppingModel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            "id"   => $this->getId(),
            "name" => $this->getName()
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
