<?php

namespace Pizza\Models;

/**
 * Class ResponseModel
 * @package Pizza\Models
 */
class ResponseModel
{
    /**
     * @var
     */
    private $data;
    /**
     * @var
     */
    private $statusCode;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }
}
